package com.example.product1.model.ObjectClass

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "spGioHangclass")
class SanPhamGioHang : SanPham() {
    @PrimaryKey(autoGenerate = true)
    var mId : Int = 0

    @ColumnInfo(name = "so_luong_mua")
    var mSoLuongMua : Int = 0
}