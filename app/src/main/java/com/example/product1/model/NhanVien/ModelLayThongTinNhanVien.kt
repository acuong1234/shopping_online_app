package com.example.product1.model.NhanVien

import android.util.Log
import com.example.product1.connectinternet.DownloadJson
import com.example.product1.model.ObjectClass.Link
import com.example.product1.model.ObjectClass.NhanVien
import com.example.product1.model.SanPham.XuLySanPhamTheoDanhMuc
import org.json.JSONObject
import java.util.concurrent.ExecutionException

class ModelLayThongTinNhanVien {
    fun LayThongTinNV(tendn: String): NhanVien {
        var nhanVien = NhanVien()
        val link = Link()
        val duongdan = link.link
        val attrs = ArrayList<HashMap<String, String>>()

        val hsHam = HashMap<String, String>()
        hsHam["ham"] = "LayThongTinNV"

        val hsTendn = HashMap<String, String>()
        hsTendn["tendn"] = tendn

        attrs.add(hsHam)
        attrs.add(hsTendn)

        val downloadJson = DownloadJson(duongdan, attrs)
        downloadJson.execute()

        var dataJson = ""

        try {
            dataJson = downloadJson.get()
            Log.d("testJson",dataJson)
            nhanVien = parseJsonNV(dataJson)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        } catch (e: ExecutionException) {
            e.printStackTrace()
        }


        return nhanVien
    }

    fun parseJsonNV(dataJson: String): NhanVien {
        var nhanVien = NhanVien()
        val jsonObject = JSONObject(dataJson)
        val jsonArray = jsonObject.getJSONArray("NHANVIEN")
        for (i in 0 until jsonArray.length()) {
            val value = jsonArray.getJSONObject(i)
            nhanVien.mMaNV = value.getInt("MANV")
            nhanVien.mTenNV = value.getString("TENNV")
            nhanVien.mDiaChi = value.getString("DIACHI")
            nhanVien.mSoDT = value.getString("SODT")
        }

        return nhanVien
    }
}