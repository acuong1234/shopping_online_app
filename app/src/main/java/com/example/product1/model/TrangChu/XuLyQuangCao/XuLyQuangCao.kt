package com.example.product1.model.TrangChu.XuLyQuangCao

import com.example.product1.model.ObjectClass.QuangCao
import org.json.JSONException
import org.json.JSONObject

class XuLyQuangCao {
    public fun parseJsonQC(dataJson:String) : ArrayList<QuangCao>{
        var listQC = ArrayList<QuangCao>()
        try{
            val jSONObject = JSONObject(dataJson)
            val jsonArray = jSONObject.getJSONArray("QUANGCAO")
            val count = jsonArray.length()
            for(i in 0 until count){
                val value = jsonArray.getJSONObject(i)

                var quangCao = QuangCao()
                quangCao.mAnhQC = value.getString("ANHQC")
                quangCao.mLinkQC = value.getString("LINKQC")

                listQC.add(quangCao)
            }

        }catch (e: JSONException){

        }


        return listQC
    }
}