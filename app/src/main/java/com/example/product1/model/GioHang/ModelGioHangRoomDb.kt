package com.example.product1.model.GioHang

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteOpenHelper
import com.example.product1.model.ObjectClass.SanPhamGioHang

@Database(entities = arrayOf(SanPhamGioHang::class), version = 1, exportSchema = false)
abstract class ModelGioHangRoomDb : RoomDatabase() {
    abstract fun noteDao(): SanPhamGioHangDao

    companion object {
        @Volatile
        private var INSTANCE : ModelGioHangRoomDb?= null
        fun getDatabase(context: Context): ModelGioHangRoomDb {
            return INSTANCE ?: synchronized(this){
                INSTANCE
                    ?: buildDatabase(context).also { INSTANCE = it }
            }
        }

        fun buildDatabase(context: Context): ModelGioHangRoomDb {
            return Room.databaseBuilder(context, ModelGioHangRoomDb::class.java,"db").build()
        }
    }
}