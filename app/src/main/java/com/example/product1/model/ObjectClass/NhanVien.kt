package com.example.product1.model.ObjectClass

class NhanVien{
    var mMaNV : Int = 0
    var mTenNV : String = ""
    var mTenDangNhap : String = ""
    var mMatKhau : String = ""
    var mDiaChi : String = ""
    var mNgaySinh : String =""
    var mSoDT : String =""
    var mMaLoaiNV : Int = 0
    var mEmailDocQuyen : String = ""
    var mFromFb : Int = 0
}