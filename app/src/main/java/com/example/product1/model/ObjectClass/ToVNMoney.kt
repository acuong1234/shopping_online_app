package com.example.product1.model.ObjectClass

import java.text.NumberFormat
import java.util.*

class ToVNMoney() {
    fun toVNMoneyFunction(price : Int):String{
        val localeVN = Locale("vi", "VN")
        val currencyVN = NumberFormat.getCurrencyInstance(localeVN)
        val strGia = currencyVN.format(price)
        return strGia
    }
}