package com.example.product1.model.ObjectClass

import java.io.Serializable

class DonHang(
    var mMaDonHang : String = "",
    var mMaNguoiNhan : String = "",
    var mTenNguoiNhan : String = "",
    var mDiaChiNhan : String = "",
    var mSoDTNhan : String = "",
    var mGioDat: String = "",
    var mTrangThai : String = "",
    var mTongTien : Int = 0,
    var mChiTietDonHang : ArrayList<SanPhamGioHang> = ArrayList<SanPhamGioHang>()
) : Serializable