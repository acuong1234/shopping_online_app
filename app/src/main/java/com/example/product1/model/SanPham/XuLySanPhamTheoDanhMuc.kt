package com.example.product1.model.SanPham

import com.example.product1.model.ObjectClass.SanPham
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class XuLySanPhamTheoDanhMuc {
    public fun parseJsonSanPham(dulieuJson:String):ArrayList<SanPham> {
        var listSanPham = ArrayList<SanPham>()
        try {
            val jsonObject = JSONObject(dulieuJson)
            val jsonArray: JSONArray = jsonObject.getJSONArray("SANPHAM")
            val count:Int = jsonArray.length()
            for (i in 0 until  count){
                val value = jsonArray.getJSONObject(i)

                var sanPham: SanPham = SanPham()

                sanPham.mMaSP = Integer.parseInt(value.getString("MASP"))
                sanPham.mTenSP = value.getString("TENSP")
                sanPham.mGia = Integer.parseInt(value.getString("GIA"))
                sanPham.mAnhNho = value.getString("ANHNHO")
                sanPham.mAnhLon = value.getString("ANHLON")
                sanPham.mThongTin = value.getString("THONGTIN")

                listSanPham.add(sanPham)
            }

        }catch (e: JSONException){

        }

        return listSanPham
    }
}