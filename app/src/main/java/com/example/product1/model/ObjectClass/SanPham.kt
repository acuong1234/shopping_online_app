package com.example.product1.model.ObjectClass

import androidx.room.ColumnInfo
import java.io.Serializable

open class SanPham : Serializable {
    @ColumnInfo(name = "ma_sp")
    var mMaSP : Int = 0

    @ColumnInfo(name = "ten_sp")
    var mTenSP : String = ""

    @ColumnInfo(name = "gia")
    var mGia : Int = 0

    @ColumnInfo(name = "anh_lon")
    var mAnhLon : String = ""

    @ColumnInfo(name = "anh_nho")
    var mAnhNho: String = ""

    @ColumnInfo(name = "thong_tin")
    var mThongTin : String = ""

    @ColumnInfo(name = "so_luong")
    var mSoLuong : Int = 0

    @ColumnInfo(name = "ma_loai_sp")
    var mMaLoaiSP : Int = 0

    @ColumnInfo(name = "ma_thuong_hieu")
    var mMaThuongHieu : Int = 0

    @ColumnInfo(name = "luot_mua")
    var mLuotMua : Int = 0
}