package com.example.product1.model.TrangChu.XuLyMenu

import android.util.Log
import com.example.product1.R
import com.example.product1.connectinternet.DownloadJson
import com.example.product1.model.ObjectClass.Link
import com.example.product1.model.ObjectClass.LoaiSanPham
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap
import java.util.concurrent.ExecutionException

class XuLyJsonMenu {


    public fun parseJsonMenu(dulieuJson:String):ArrayList<LoaiSanPham>{
        var listLoaiSanPham = ArrayList<LoaiSanPham>()
        try {
            val jsonObject = JSONObject(dulieuJson)
            val jsonArray:JSONArray = jsonObject.getJSONArray("LOAISANPHAM")
            val count:Int = jsonArray.length()
            for (i in 0 until  count){
                val value = jsonArray.getJSONObject(i)

                var loaiSanPham:LoaiSanPham=LoaiSanPham()
                loaiSanPham.mMaLoaiCha=Integer.parseInt(value.getString("MALOAI_CHA"))
                loaiSanPham.mMaLoaiSP=Integer.parseInt(value.getString("MALOAISP"))
                loaiSanPham.mTenLoaiSP=value.getString("TENLOAISP")
                loaiSanPham.mListCon = layDanhSachTheoMaLoai(Integer.parseInt(value.getString("MALOAISP")))
                listLoaiSanPham.add(loaiSanPham)
            }

        }catch (e:JSONException){
            e.printStackTrace()
        }
        return listLoaiSanPham
    }

    public fun layDanhSachTheoMaLoai(maloaisp:Int): List<LoaiSanPham> {
        var loaiSanPhamList: List<LoaiSanPham> = java.util.ArrayList()
        val attrs = java.util.ArrayList<HashMap<String, String>>()
        var dataJSON = ""

        val link = Link()
        val duongdan = link.link

        val hsHam = HashMap<String, String>()
        hsHam["ham"] = "LayDanhSachMenu"

        val hsMaLoaiCha = HashMap<String, String>()
        hsMaLoaiCha["maloaicha"] = maloaisp.toString()

        attrs.add(hsMaLoaiCha)
        attrs.add(hsHam)

        val downloadJSON = DownloadJson(duongdan, attrs)
        //end phương thức post
        downloadJSON.execute()

        try {
            dataJSON = downloadJSON.get()
            val xuLyJSONMenu = XuLyJsonMenu()
            loaiSanPhamList = xuLyJSONMenu.parseJsonMenu(dataJSON)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        } catch (e: ExecutionException) {
            e.printStackTrace()
        }


        return loaiSanPhamList
    }
}