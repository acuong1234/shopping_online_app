package com.example.product1.model.DangNhap

import android.util.Log
import com.example.product1.connectinternet.DownloadJson
import com.example.product1.model.ObjectClass.Link
import com.example.product1.model.ObjectClass.NhanVien
import org.json.JSONObject
import java.util.concurrent.ExecutionException

class ModelDangNhap {
    fun DangNhap(tendangnhap:String,matkhau:String):String{
        var checked = false
        var nhanVien : String = ""
        val link = Link()
        val duongdan = link.link
        val attrs = ArrayList<HashMap<String,String>>()

        val hsHam = HashMap<String,String>()
        hsHam["ham"]= "KiemTraDangNhap"

        val hsTendangnhap = HashMap<String,String>()
        hsTendangnhap["tendangnhap"] = tendangnhap

        val hsMatkhau = HashMap<String, String>()
        hsMatkhau["matkhau"] = matkhau

        attrs.add(hsHam)
        attrs.add(hsTendangnhap)
        attrs.add(hsMatkhau)

        val downloadJSON = DownloadJson(duongdan, attrs)
        downloadJSON.execute()

        var dataJson = ""
        try{
            dataJson = downloadJSON.get()
            Log.d("testDN",dataJson)
            val jsonObject = JSONObject(dataJson)
            val data = jsonObject.getString("ketqua")
            if(data.equals("true")){
                nhanVien = jsonObject.getString("tennv")
            }else{
                nhanVien = ""
            }
        }catch (e:InterruptedException){

        }catch (e: ExecutionException){

        }

        return nhanVien
    }
}