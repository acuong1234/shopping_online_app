package com.example.product1.model.DonHang

import android.util.Log
import com.example.product1.connectinternet.DownloadJson
import com.example.product1.model.ObjectClass.DonHang
import com.example.product1.model.ObjectClass.Link
import org.json.JSONObject
import java.util.concurrent.ExecutionException

class ModelTaoDonHang {
    fun TaoDonHang(donHang: DonHang):Boolean{
        var check : Boolean = false
        val link = Link()
        val duongdan = link.link
        val attrs = ArrayList<HashMap<String,String>>()

        val hsHam = HashMap<String,String>()
        hsHam["ham"] = "TaoDonHang"

        val hsMaDH = HashMap<String,String>()
        hsMaDH["madh"] = donHang.mMaDonHang

        val hsMaNguoiNhan = HashMap<String,String>()
        hsMaNguoiNhan["manguoinhan"] = donHang.mMaNguoiNhan

        val hsTenNguoiNhan = HashMap<String,String>()
        hsTenNguoiNhan["tennguoinhan"] = donHang.mTenNguoiNhan

        val hsDiaChiNhan = HashMap<String,String>()
        hsDiaChiNhan["diachinhan"] = donHang.mDiaChiNhan

        val hsSDTNhan = HashMap<String,String>()
        hsSDTNhan["sdtnhan"] = donHang.mSoDTNhan

        val hsArraySize = HashMap<String,String>()
        hsArraySize["size"] = donHang.mChiTietDonHang.size.toString()

        val hsTimeOrder = HashMap<String,String>()
        hsTimeOrder["time"] = donHang.mGioDat

        val hsTrangThai = HashMap<String,String>()
        hsTrangThai["trangthai"] = donHang.mTrangThai

        val hsTongTien = HashMap<String,String>()
        hsTongTien["tongtien"] = donHang.mTongTien.toString()

        attrs.add(hsHam)
        attrs.add(hsMaDH)
        attrs.add(hsTenNguoiNhan)
        attrs.add(hsDiaChiNhan)
        attrs.add(hsTenNguoiNhan)
        attrs.add(hsArraySize)
        attrs.add(hsSDTNhan)
        attrs.add(hsTimeOrder)
        attrs.add(hsMaNguoiNhan)
        attrs.add(hsTrangThai)
        attrs.add(hsTongTien)

        Log.d("testDk",donHang.mChiTietDonHang.size.toString())
        for(i in 0 until donHang.mChiTietDonHang.size){
            val hsChiTietMaSP = HashMap<String,String>()
            val hsChiTietSoLuong = HashMap<String,String>()
            hsChiTietMaSP["masp"+i] = donHang.mChiTietDonHang.get(i).mMaSP.toString()
            hsChiTietSoLuong["soluong"+i] = donHang.mChiTietDonHang.get(i).mSoLuongMua.toString()
            attrs.add(hsChiTietMaSP)
            attrs.add(hsChiTietSoLuong)
        }

        val downloadJson = DownloadJson(duongdan,attrs)
        downloadJson.execute()

        var dataJson = ""
        try{
            dataJson = downloadJson.get()
            Log.d("testDk",dataJson)
            val jsonObject = JSONObject(dataJson)
            val data : String = jsonObject.optString("ketqua")
            if(data.equals("true")){
                check = true
            }
        }catch (e:InterruptedException){

        }catch (e: ExecutionException){

        }
        return check
    }
}