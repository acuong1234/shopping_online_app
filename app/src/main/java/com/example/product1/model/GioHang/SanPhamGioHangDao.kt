package com.example.product1.model.GioHang

import androidx.room.*
import com.example.product1.model.ObjectClass.SanPhamGioHang

@Dao
abstract interface SanPhamGioHangDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(note: SanPhamGioHang)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun Update(note: SanPhamGioHang)

    @Delete
    fun delete(vararg note: SanPhamGioHang)

    @Query("DELETE FROM spGioHangclass" )
    fun deletAll()

    @Query("SELECT * FROM spGioHangclass ")
    fun getAll(): List<SanPhamGioHang>

}