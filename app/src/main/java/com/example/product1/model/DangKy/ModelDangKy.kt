package com.example.product1.model.DangKy

import android.util.Log
import com.example.product1.R
import com.example.product1.connectinternet.DownloadJson
import com.example.product1.model.ObjectClass.NhanVien
import com.example.product1.model.ObjectClass.Link
import org.json.JSONObject
import java.util.concurrent.ExecutionException

class ModelDangKy  {
    fun DangKyThanhVien(nhanVien: NhanVien) : Boolean{
        var check : Boolean = false
        val link = Link()
        val duongdan = link.link
        val attrs = ArrayList<HashMap<String,String>>()

        val hsHam = HashMap<String,String>()
        hsHam["ham"]= "DangKyThanhVien"

        val hsTennv = HashMap<String, String>()
        hsTennv["tennv"] = nhanVien.mTenNV

        val hsTendangnhap = HashMap<String,String>()
        hsTendangnhap["tendangnhap"] = nhanVien.mTenDangNhap

        val hsMatkhau = HashMap<String, String>()
        hsMatkhau["matkhau"] = nhanVien.mMatKhau

        val hsMaloainhanvien = HashMap<String, String>()
        hsMaloainhanvien["maloainv"] = nhanVien.mMaLoaiNV.toString()

        val hsEmaildocquyen = HashMap<String, String>()
        hsEmaildocquyen["emaildocquyen"] = nhanVien.mEmailDocQuyen

        val hsFromfb = HashMap<String, String>()
        hsFromfb["fromfb"] = nhanVien.mFromFb.toString()

        attrs.add(hsHam)
        attrs.add(hsTennv)
        attrs.add(hsTendangnhap)
        attrs.add(hsMatkhau)
        attrs.add(hsMaloainhanvien)
        attrs.add(hsEmaildocquyen)
        attrs.add(hsFromfb)

        val downloadJSON = DownloadJson(duongdan, attrs)
        downloadJSON.execute()

        var dataJson = ""
        try{
            dataJson = downloadJSON.get()
            Log.d("testDk",dataJson)
            val jsonObject = JSONObject(dataJson)
            val data : String = jsonObject.getString("ketqua")
            if(data.equals("true")){
                check = true
            }
        }catch (e:InterruptedException){

        }catch (e:ExecutionException){

        }
        return check
    }


}