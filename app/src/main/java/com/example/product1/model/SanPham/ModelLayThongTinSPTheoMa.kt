package com.example.product1.model.SanPham

import android.util.Log
import com.example.product1.connectinternet.DownloadJson
import com.example.product1.model.ObjectClass.DonHang
import com.example.product1.model.ObjectClass.Link
import com.example.product1.model.ObjectClass.SanPham
import org.json.JSONObject
import java.lang.Exception

class ModelLayThongTinSPTheoMa {
    fun LayThongTinSPTheoMa(masp : Int):SanPham{
        var sanPham = SanPham()
        val link = Link()
        val duongdan = link.link
        val attrs = ArrayList<HashMap<String,String>>()

        val hsHam = HashMap<String,String>()
        hsHam["ham"] = "LayThongTinSPTheoMa"

        val hsMaSP = HashMap<String,String>()
        hsMaSP["masp"] = masp.toString()

        attrs.add(hsHam)
        attrs.add(hsMaSP)

        val downloadJson = DownloadJson(duongdan,attrs)
        downloadJson.execute()

        var dataJson = ""
        try {
            dataJson = downloadJson.get()
            Log.d("TestJS",dataJson)
            sanPham = parseJsonSP(dataJson)
        }catch (e: InterruptedException){

        }catch (e: Exception){

        }

        return sanPham
    }

    fun parseJsonSP(dataJson:String):SanPham{
        var sanPham = SanPham()
        val jsonObject = JSONObject(dataJson)
        val jsonArray = jsonObject.getJSONArray("SANPHAM")
        for(i in 0 until jsonArray.length()){
            val value = jsonArray.getJSONObject(i)

            sanPham.mMaSP = value.getInt("MASP")
            sanPham.mTenSP = value.getString("TENSP")
            sanPham.mGia = value.getInt("GIA")
            sanPham.mAnhNho = value.getString("ANHNHO")
            sanPham.mAnhLon = value.getString("ANHLON")
            sanPham.mThongTin = value.getString("THONGTIN")
        }
        return sanPham
    }
}