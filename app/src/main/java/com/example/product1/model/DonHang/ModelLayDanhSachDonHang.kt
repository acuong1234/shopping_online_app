package com.example.product1.model.DonHang

import android.util.Log
import com.example.product1.connectinternet.DownloadJson
import com.example.product1.model.ObjectClass.DonHang
import com.example.product1.model.ObjectClass.Link
import com.example.product1.model.ObjectClass.SanPhamGioHang
import com.example.product1.model.SanPham.ModelLayThongTinSPTheoMa
import org.json.JSONObject
import java.lang.Exception

class ModelLayDanhSachDonHang {
    fun LayDanhSachDonHang(manv : Int):ArrayList<DonHang>{
        var listDonHang = ArrayList<DonHang>()
        val link = Link()
        val duongdan = link.link
        val attrs = ArrayList<HashMap<String,String>>()

        val hsHam = HashMap<String,String>()
        hsHam["ham"] = "LayDanhSachDonHang"

        val hsMaNV = HashMap<String,String>()
        hsMaNV["manv"] = manv.toString()

        attrs.add(hsHam)
        attrs.add(hsMaNV)

        val downloadJson = DownloadJson(duongdan,attrs)
        downloadJson.execute()

        var dataJson = ""
        try {
            dataJson = downloadJson.get()
            Log.d("TestJS",dataJson)
            listDonHang = parseJsonDH(dataJson)
        }catch (e: InterruptedException){

        }catch (e: Exception){

        }
        Log.d("TestJS",listDonHang.size.toString())
        return listDonHang
    }

    fun parseJsonDH(dataJson : String):ArrayList<DonHang>{
        var listDonHang = ArrayList<DonHang>()
        val jsonObject = JSONObject(dataJson)
        val jsonArray = jsonObject.getJSONArray("DONHANG")
        for(i in 0 until jsonArray.length()){
            val value = jsonArray.getJSONObject(i)

            val donHang = DonHang()
            donHang.mMaDonHang = value.getString("MADONHANG")
            donHang.mMaNguoiNhan = value.getString("MANV")
            donHang.mTenNguoiNhan = value.getString("TENNGUOINHAN")
            donHang.mDiaChiNhan = value.getString("DIACHINHAN")
            donHang.mSoDTNhan = value.getString("SDTNHAN")
            donHang.mGioDat = value.getString("GIODAT")
            donHang.mTrangThai = value.getString("TRANGTHAI")
            donHang.mTongTien = value.getInt("TONGTIEN")
            donHang.mChiTietDonHang = LayChiTietDonHang(donHang.mMaDonHang)

            listDonHang.add(donHang)
        }
        return listDonHang
    }

    fun LayChiTietDonHang(madh : String):ArrayList<SanPhamGioHang>{
        var listSP = ArrayList<SanPhamGioHang>()
        val link = Link()
        val duongdan = link.link
        val attrs = ArrayList<HashMap<String,String>>()

        val hsHam = HashMap<String,String>()
        hsHam["ham"] = "LayChiTietDonHang"

        val hsMaDH = HashMap<String,String>()
        hsMaDH["madh"] = madh

        attrs.add(hsHam)
        attrs.add(hsMaDH)

        val downloadJson = DownloadJson(duongdan,attrs)
        downloadJson.execute()

        var dataJson = ""
        try {
            dataJson = downloadJson.get()
            Log.d("TestJS",dataJson)
            listSP = parseJsonSP(dataJson)
        }catch (e: InterruptedException){

        }catch (e: Exception){

        }

        return listSP
    }

    fun parseJsonSP(dataJson: String):ArrayList<SanPhamGioHang>{
        var listSP1 = ArrayList<SanPhamGioHang>()
        val jsonObject = JSONObject(dataJson)
        val jsonArray = jsonObject.getJSONArray("SP")
        for(i in 0 until jsonArray.length()){
            val value = jsonArray.getJSONObject(i)

            var sanPhamGioHang = SanPhamGioHang()
            val sanPham = ModelLayThongTinSPTheoMa().LayThongTinSPTheoMa(value.getInt("MASP"))
            sanPhamGioHang.mMaSP = sanPham.mMaSP
            sanPhamGioHang.mTenSP = sanPham.mTenSP
            sanPhamGioHang.mGia = sanPham.mGia
            sanPhamGioHang.mAnhNho = sanPham.mAnhNho
            sanPhamGioHang.mAnhLon = sanPham.mAnhLon
            sanPhamGioHang.mThongTin = sanPham.mThongTin
            sanPhamGioHang.mSoLuongMua = value.getInt("SOLUONGSP")

            listSP1.add(sanPhamGioHang)
        }
        return  listSP1
    }
}