package com.example.product1.view.GioHang

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.room.Room
import com.example.product1.R
import com.example.product1.adapter.DataChangeGioHang
import com.example.product1.adapter.ListGioHangAdapter
import com.example.product1.model.GioHang.ModelGioHangRoomDb
import com.example.product1.model.ObjectClass.DonHang
import com.example.product1.model.ObjectClass.NhanVien
import com.example.product1.model.ObjectClass.SanPhamGioHang
import com.example.product1.model.ObjectClass.ToVNMoney
import com.example.product1.presenter.DonHang.PresenterLogicTaoDonHang
import com.example.product1.presenter.GioHang.PresenterLogicGioHang
import com.example.product1.presenter.NhanVien.PresenterLogicLayThongTinNV
import com.example.product1.view.TrangChu.TrangChu
import com.example.product1.view.ViewXuLyNhanVien
import kotlinx.android.synthetic.main.activity_gio_hang.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class GioHangActivity : AppCompatActivity(),ViewXuLyNhanVien,ViewTaoDonHang {
    override fun TaoThanhCong() {
        Toast.makeText(applicationContext,"Thành công",Toast.LENGTH_SHORT).show()
        view_giohang.visibility = View.INVISIBLE
        noti_succes.visibility = View.VISIBLE
        presenterLogicGioHang!!.deletaAll()
    }

    override fun TaoThatBai() {
        Toast.makeText(applicationContext,"Thất bại",Toast.LENGTH_SHORT).show()
    }

    override fun HienThiThongTin(nhanVien: NhanVien) {
        txt_dia_chi_nhan.setText(nhanVien.mDiaChi)
        txt_sdt_nhan.append(nhanVien.mSoDT)
        diachi = nhanVien.mDiaChi
        sdt = nhanVien.mSoDT
        manv = nhanVien.mMaNV.toString()
    }

    var diachi : String = ""
    var sdt : String =""
    var manv : String =""
    var madonhang : String = ""
    var tongTien: Int = 0
    var presenterLogicLayThongTinNV : PresenterLogicLayThongTinNV? = null
    var presenterLogicTaoDonHang : PresenterLogicTaoDonHang? = null
    var presenterLogicGioHang : PresenterLogicGioHang? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gio_hang)

        val db = Room.databaseBuilder(
            applicationContext,
            ModelGioHangRoomDb::class.java, "db"
        ).build()

        presenterLogicGioHang = PresenterLogicGioHang(db.noteDao())
        TrangChu.listBuy = presenterLogicGioHang!!.getAll() as ArrayList<SanPhamGioHang>

        val tendn : String = getSharedPreferences("id", Context.MODE_PRIVATE).getString("tendn", "")
        val name : String = getSharedPreferences("id", Context.MODE_PRIVATE).getString("curUser", "")
        Log.d("testJson",tendn)

        presenterLogicLayThongTinNV = PresenterLogicLayThongTinNV(this)
        presenterLogicLayThongTinNV!!.LayThongTinNhanVien(tendn)

        view_giohang.visibility = View.VISIBLE

        if (TrangChu.listBuy.size > 0) {
            init()
            getGioHang()
        }

        presenterLogicTaoDonHang = PresenterLogicTaoDonHang(this)

        btn_order.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Xác nhận đặt hàng")
            builder.setMessage("Nhấn Xác nhận để hoàn tất việc đặt hàng")
            builder.setNegativeButton("Hủy",object : DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    dialog!!.dismiss()
                }
            })
            builder.setPositiveButton("Xác nhận",object : DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    val dateFormat = SimpleDateFormat("ddMMyyHHmmss")
                    dateFormat.isLenient = false
                    madonhang = manv+dateFormat.format(Date())
                    val donhang = DonHang(madonhang,manv,name,diachi,sdt,dateFormat.format(Date()),"3",tongTien,TrangChu.listBuy)
                    presenterLogicTaoDonHang!!.TaoDonHang(donhang)
                    dialog!!.dismiss()
                }
            })
            val dialog = builder.create()
            dialog.show()
        }

        btn_back.setOnClickListener {
            finish()
        }
    }

    fun init() {
        txt_empty.visibility = View.INVISIBLE
        list_item_giohang.visibility = View.VISIBLE
        txt_total_price.visibility = View.VISIBLE
        btn_order.visibility = View.VISIBLE
        txt_tong_tien_ban.visibility = View.VISIBLE
    }

    fun empty() {
        txt_empty.visibility = View.VISIBLE
        list_item_giohang.visibility = View.INVISIBLE
        txt_total_price.visibility = View.INVISIBLE
        btn_order.visibility = View.INVISIBLE
        txt_tong_tien_ban.visibility = View.INVISIBLE
    }

    fun getGioHang() {
        val db = Room.databaseBuilder(
            applicationContext,
            ModelGioHangRoomDb::class.java, "db"
        ).build()
        val presenterLogicGioHang = PresenterLogicGioHang(db.noteDao())
        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(applicationContext)
        val listGioHangAdapter = ListGioHangAdapter(applicationContext, TrangChu.listBuy,presenterLogicGioHang, object : DataChangeGioHang {
            override fun dataIncrease(data: Int, pos: Int) {
                tongTien += data
                txt_total_price.setText(ToVNMoney().toVNMoneyFunction(tongTien))
            }

            override fun dataDecrease(data: Int, pos: Int) {
                tongTien -= data
                txt_total_price.setText(ToVNMoney().toVNMoneyFunction(tongTien))
                if (tongTien == 0)
                    empty()
            }

        })
        list_item_giohang.layoutManager = layoutManager
        list_item_giohang.adapter = listGioHangAdapter
        list_item_giohang.setHasFixedSize(true)
        list_item_giohang.isFocusable = false
        list_item_giohang.isNestedScrollingEnabled = false
        listGioHangAdapter.notifyDataSetChanged()

        for (i in 0 until TrangChu.listBuy.size)
            tongTien += (TrangChu.listBuy.get(i).mSoLuongMua * TrangChu.listBuy.get(i).mGia)
        txt_total_price.setText(ToVNMoney().toVNMoneyFunction(tongTien))
    }
}
