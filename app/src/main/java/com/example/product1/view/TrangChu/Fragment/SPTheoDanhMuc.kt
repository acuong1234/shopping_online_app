package com.example.product1.view.TrangChu.Fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.product1.R
import com.example.product1.adapter.ClickItemList
import com.example.product1.adapter.RecycleApdater
import com.example.product1.model.ObjectClass.SanPham
import com.example.product1.presenter.SanPham.PresenterLogicSanPhamTheoDanhMuc
import com.example.product1.view.ChitietSP.ChiTietSanPham
import com.example.product1.view.TrangChu.ViewSPTheoDanhMuc

class SPTheoDanhMuc : androidx.fragment.app.Fragment(),ViewSPTheoDanhMuc {

    override fun HienThiSPTheoDanhMuc(listSanPham: ArrayList<SanPham>) {
        getList(listSanPham)
    }

    var recyclerView : androidx.recyclerview.widget.RecyclerView? = null
    var presenterLogicSanPhamTheoDanhMuc : PresenterLogicSanPhamTheoDanhMuc? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view: View = inflater.inflate(R.layout.fragment_noibat,container,false)

        recyclerView = view.findViewById(R.id.rv_noibat)
        presenterLogicSanPhamTheoDanhMuc = PresenterLogicSanPhamTheoDanhMuc(this)
        presenterLogicSanPhamTheoDanhMuc!!.LayDanhSachSPTheoDanhMuc(2)

        return view
    }

    fun getList(listSanPham: ArrayList<SanPham>){
        val layoutManager = androidx.recyclerview.widget.GridLayoutManager(context, 2)
        val adapter:RecycleApdater = RecycleApdater(context,listSanPham,object : ClickItemList{
            override fun onClick(position: Int) {
                val intent: Intent = Intent(context,ChiTietSanPham::class.java)
                intent.putExtra("item",listSanPham.get(position))
                startActivity(intent)
            }

        })
        recyclerView!!.layoutManager = layoutManager
        recyclerView!!.adapter = adapter
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.isFocusable = false
        recyclerView!!.isNestedScrollingEnabled = false
    }
}