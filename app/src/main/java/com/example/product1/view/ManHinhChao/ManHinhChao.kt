package com.example.product1.view.ManHinhChao

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.product1.R
import com.example.product1.view.TrangChu.TrangChu
import java.lang.Exception

class ManHinhChao : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.man_hinh_chao)

        var thread: Thread = Thread(Runnable {
            run {
                try {
                    Thread.sleep(3000)

                }catch (e: Exception){

                }finally {
                    val intent:Intent = Intent(applicationContext,TrangChu::class.java)
                    startActivity(intent)
                }
            }
        })

        thread.start()
    }
}
