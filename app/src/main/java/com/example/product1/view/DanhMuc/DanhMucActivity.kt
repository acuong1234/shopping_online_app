package com.example.product1.view.DanhMuc

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.product1.R
import com.example.product1.adapter.ClickItemList
import com.example.product1.adapter.RecycleApdater
import com.example.product1.model.ObjectClass.DanhMuc
import com.example.product1.model.ObjectClass.SanPham
import com.example.product1.presenter.SanPham.PresenterLogicSanPhamTheoDanhMuc
import com.example.product1.view.ChitietSP.ChiTietSanPham
import com.example.product1.view.TrangChu.ViewSPTheoDanhMuc
import kotlinx.android.synthetic.main.activity_danh_muc.*

class DanhMucActivity : AppCompatActivity(), ViewSPTheoDanhMuc {
    override fun HienThiSPTheoDanhMuc(listSanPham: ArrayList<SanPham>) {
        getSpDanhMuc(listSanPham)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_danh_muc)

        val intent = intent
        val danhmuc: DanhMuc = intent.getSerializableExtra("danhmuc") as DanhMuc

        tb_danhmuc.setTitle(danhmuc.mName)
        setSupportActionBar(tb_danhmuc)


        var presenterLogicSanPhamTheoDanhMuc: PresenterLogicSanPhamTheoDanhMuc? = null
        presenterLogicSanPhamTheoDanhMuc = PresenterLogicSanPhamTheoDanhMuc(this)
        presenterLogicSanPhamTheoDanhMuc.LayDanhSachSPTheoDanhMuc(danhmuc.mMaDanhMuc)


    }

    fun getSpDanhMuc(listSanPham: ArrayList<SanPham>) {
        val layoutManager = androidx.recyclerview.widget.GridLayoutManager(applicationContext, 2)
        val adapter: RecycleApdater = RecycleApdater(applicationContext, listSanPham, object : ClickItemList {
            override fun onClick(position: Int) {
                val intent: Intent = Intent(applicationContext, ChiTietSanPham::class.java)
                intent.putExtra("item", listSanPham.get(position))
                startActivity(intent)
            }
        })
        list_sp_danhmuc.layoutManager = layoutManager
        list_sp_danhmuc.adapter = adapter
        list_sp_danhmuc.setHasFixedSize(true)
    }
}
