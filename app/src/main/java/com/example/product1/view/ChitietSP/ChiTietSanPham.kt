package com.example.product1.view.ChitietSP

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.product1.R
import com.example.product1.adapter.ClickItemList
import com.example.product1.adapter.SliderAdapter
import com.example.product1.adapter.SliderSPAdapter
import com.example.product1.model.GioHang.ModelGioHangRoomDb
import com.example.product1.model.ObjectClass.SanPham
import com.example.product1.model.ObjectClass.SanPhamGioHang
import com.example.product1.model.ObjectClass.ToVNMoney
import com.example.product1.presenter.GioHang.PresenterLogicGioHang
import com.example.product1.view.TrangChu.TrangChu
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_chi_tiet_san_pham.*
import kotlinx.android.synthetic.main.activity_trang_chu.*
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class ChiTietSanPham : AppCompatActivity() {
    var presenterLogicGioHang: PresenterLogicGioHang? = null
    var sanpham: SanPham? = null
    var checkExist: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chi_tiet_san_pham)

        val db = Room.databaseBuilder(
            applicationContext,
            ModelGioHangRoomDb::class.java, "db"
        ).build()
        presenterLogicGioHang = PresenterLogicGioHang(db.noteDao())

        val intent = intent
        sanpham = intent.getSerializableExtra("item") as SanPham
        val listAnh : List<String> = sanpham!!.mAnhLon.split(",")
        getSliderImg(listAnh)
        tb_chitietsp.setTitle(sanpham!!.mTenSP)
        txt_name.setText(sanpham!!.mTenSP)
        txt_gia.setText(ToVNMoney().toVNMoneyFunction(sanpham!!.mGia))
        txt_chitietsp.setText(sanpham!!.mThongTin)

        btn_add_to_shoppingcart.setOnClickListener {
            TrangChu.listBuy = presenterLogicGioHang!!.getAll() as ArrayList<SanPhamGioHang>
            for (SanPhamGioHang in TrangChu.listBuy) {
                if (SanPhamGioHang.mMaSP == sanpham!!.mMaSP) {
                    SanPhamGioHang.mSoLuongMua++
                    presenterLogicGioHang!!.update(SanPhamGioHang)
                    checkExist = true
                    break;
                } else {
                    checkExist = false
                }
            }
            if (checkExist == false) {
                val sanPhamGioHang = SanPhamGioHang()
                sanPhamGioHang.mTenSP = sanpham!!.mTenSP
                sanPhamGioHang.mAnhNho = sanpham!!.mAnhNho
                sanPhamGioHang.mMaSP = sanpham!!.mMaSP
                sanPhamGioHang.mGia = sanpham!!.mGia
                sanPhamGioHang.mSoLuongMua = 1

                presenterLogicGioHang!!.insert(sanPhamGioHang)
            }
            Toast.makeText(applicationContext, "Đã thêm vào giỏ hàng", Toast.LENGTH_SHORT).show()
        }
    }

    fun getSliderImg(listImg : List<String>){
        sliderSP.adapter = SliderSPAdapter(applicationContext, listImg)
        indicatorSP.setViewPager(sliderSP)
    }
}
