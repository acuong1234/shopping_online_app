package com.example.product1.view.TrangChu

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.product1.R
import com.example.product1.adapter.SliderAdapter
import com.example.product1.view.DangNhapDangKi.DangNhapDangKi
import com.example.product1.view.TimKiem.TimKiem
import com.facebook.AccessToken
import com.facebook.login.LoginManager
import kotlinx.android.synthetic.main.activity_trang_chu.*
import android.os.Handler
import com.example.product1.view.TrangChu.Fragment.SPTheoDanhMuc
import java.util.*
import kotlin.collections.ArrayList
import android.net.Uri
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.room.Room
import com.example.product1.adapter.ClickItemList
import com.example.product1.adapter.ListDanhMucAdapter
import com.example.product1.model.GioHang.ModelGioHangRoomDb
import com.example.product1.model.ObjectClass.DanhMuc
import com.example.product1.model.ObjectClass.NhanVien
import com.example.product1.model.ObjectClass.QuangCao
import com.example.product1.model.ObjectClass.SanPhamGioHang
import com.example.product1.presenter.GioHang.PresenterLogicGioHang
import com.example.product1.presenter.NhanVien.PresenterLogicLayThongTinNV
import com.example.product1.presenter.TrangChu.XuLyQC.PresenterLogicQC
import com.example.product1.view.DanhMuc.DanhMucActivity
import com.example.product1.view.DonHang.DanhSachDonHang
import com.example.product1.view.GioHang.GioHangActivity
import com.example.product1.view.ViewXuLyNhanVien
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.navigation_header.*


class TrangChu : AppCompatActivity(), ViewQuangCao, ViewXuLyNhanVien {

    var isLoggedIn: Boolean = false
    var checked: Int? = 0
    var presenterLogicQC: PresenterLogicQC? = null
    var presenterLogicGioHang: PresenterLogicGioHang? = null
    var presenterLogicLayThongTinNV : PresenterLogicLayThongTinNV? = null
    var txtSl: TextView? = null
    var headername : TextView? = null

    companion object {
        var listBuy = arrayListOf<SanPhamGioHang>()
        var nameAcc: String? = ""
        var sizeListBuy: Int = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trang_chu)

        val db = Room.databaseBuilder(
            applicationContext,
            ModelGioHangRoomDb::class.java, "db"
        ).build()

        presenterLogicGioHang = PresenterLogicGioHang(db.noteDao())
        listBuy = presenterLogicGioHang!!.getAll() as ArrayList<SanPhamGioHang>

        checked = 0

        presenterLogicQC = PresenterLogicQC(this)
        presenterLogicQC!!.layDanhSachQC()

        toolbar_trangchu.setTitle("")
        setSupportActionBar(toolbar_trangchu)

        getAccount()

        getDanhMuc()

        getDrawer()

        btn_search.setOnClickListener {
            val intent = Intent(this, TimKiem::class.java)
            startActivity(intent)
        }

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        val fragment: androidx.fragment.app.Fragment = SPTheoDanhMuc()
        fragmentTransaction.replace(R.id.fragment, fragment)
        fragmentTransaction.commit()


//        actionBarDrawerToggle =
//            ActionBarDrawerToggle(this, drawerlayout, toolbar_trangchu, R.string.open, R.string.close)
//        drawerlayout.addDrawerListener(actionBarDrawerToggle!!)
//        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
//        supportActionBar!!.setDisplayShowHomeEnabled(true)
//        actionBarDrawerToggle!!.syncState()

//        getViewPager()

//        val mBuilderWait = AlertDialog.Builder(this)
//        val inflater = layoutInflater
//        val viewPleaseWait = inflater.inflate(R.layout.dialog_pleasewait, null)
//        mBuilderWait.setView(viewPleaseWait)
//        val mDialogWait :AlertDialog = mBuilderWait.create()
//        mDialogWait.show()

//        val presenterLogicXuLyMenu = PresenterLogicXuLyMenu(this)
//        presenterLogicXuLyMenu.layDanhSachMenu()

//        if(checked == 1) {
//            mDialogWait.dismiss()
//        }


//        appbar_layout.addOnOffsetChangedListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_trangchu, menu)
        val giohang: MenuItem = menu!!.findItem(R.id.item_trangchu_giohang)
        val view: View = giohang.actionView
        view.setOnClickListener {
            val intent = Intent(this, GioHangActivity::class.java)
            startActivity(intent)
        }
        txtSl = view.findViewById(R.id.txt_sl)
        sizeListBuy = 0
        for (i in 0 until listBuy.size) {
            sizeListBuy += listBuy.get(i).mSoLuongMua
        }
        if (listBuy.size == 0) {
            txtSl!!.visibility = View.INVISIBLE
        } else {
            txtSl!!.visibility = View.VISIBLE
            txtSl!!.setText(sizeListBuy.toString())
        }
        return true
    }

    fun onPrepareDrawerMenu(menu: Menu?): Boolean {
        if (isLoggedIn == true) {
            menu!!.findItem(R.id.item_trangchu_dangnhap).isVisible = false
            menu.findItem(R.id.item_trangchu_thongtin).isVisible = true
            menu.findItem(R.id.item_trangchu_dangxuat).isVisible = true
            headername!!.setText(nameAcc)

        } else {
            menu!!.findItem(R.id.item_trangchu_dangnhap).isVisible = true
            menu.findItem(R.id.item_trangchu_thongtin).isVisible = false
            menu.findItem(R.id.item_trangchu_dangxuat).isVisible = false
            headername!!.setText("Chưa đăng nhập")
        }
        return true
    }


    fun getSlider(arrayList: ArrayList<QuangCao>) {
        slider.adapter = SliderAdapter(applicationContext, arrayList, object : ClickItemList {
            override fun onClick(position: Int) {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(arrayList.get(position).mLinkQC))
                startActivity(browserIntent)
            }
        })
        var currentPage: Int = 0
        var NUM_PAGES: Int = arrayList.size
        val handler = Handler()
        val Update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            slider.setCurrentItem(currentPage++, true)
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(Update)
            }
        }, 2500, 2500)

        indicator.setViewPager(slider)
    }

    fun getDrawer() {
        headername = navigation_view.getHeaderView(0).findViewById(R.id.txt_header_name)
        val toggle = ActionBarDrawerToggle(
            this,
            my_drawer_layout,
            toolbar_trangchu,
            R.string.open_navigation,
            R.string.close_navigation
        )
        my_drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        onPrepareDrawerMenu(navigation_view.menu)
        navigation_view.setNavigationItemSelectedListener(object : NavigationView.OnNavigationItemSelectedListener {
            @SuppressLint("WrongConstant")
            override fun onNavigationItemSelected(p0: MenuItem): Boolean {
                when (p0.itemId) {
                    R.id.item_trangchu_dangnhap -> {
                        toDangNhap()
                        my_drawer_layout.closeDrawer(Gravity.START)
                        return true
                    }
                    R.id.item_trangchu_thongbao -> {
                        if (isLoggedIn == true) {
                            Toast.makeText(applicationContext, "Thông báo", Toast.LENGTH_SHORT).show()
                        } else {
                            requireDangNhap()
                        }
                        my_drawer_layout.closeDrawer(Gravity.START)
                        return true
                    }
                    R.id.item_trangchu_danhsachmongmuon -> {
                        if (isLoggedIn == true) {
                            Toast.makeText(applicationContext, "WishList", Toast.LENGTH_SHORT).show()
                        } else {
                            requireDangNhap()
                        }
                        my_drawer_layout.closeDrawer(Gravity.START)
                        return true
                    }
                    R.id.item_trangchu_donhangcuatoi -> {
                        if (isLoggedIn == true) {
                            val intent = Intent(applicationContext,DanhSachDonHang::class.java)
                            startActivity(intent)
                        } else {
                            requireDangNhap()
                        }
                        my_drawer_layout.closeDrawer(Gravity.START)
                        return true
                    }
                    R.id.item_trangchu_phienban -> {
                        Toast.makeText(applicationContext, "Phiên bản 1.0.0b", Toast.LENGTH_SHORT).show()
                        my_drawer_layout.closeDrawer(Gravity.START)
                        return true
                    }
                    R.id.item_trangchu_dangxuat -> {
                        dangXuat()
                        my_drawer_layout.closeDrawer(Gravity.START)
                        return true
                    }
                }
                return false
            }
        })
    }

    fun getDanhMuc() {
        val listDM = ArrayList<DanhMuc>()
        listDM.add(DanhMuc("Smartphone", R.drawable.ic_smartphone_black_24dp, 2))
        listDM.add(DanhMuc("Máy tính bảng", R.drawable.ic_tablet_mac_black_10dp, 37))
        listDM.add(DanhMuc("Laptop", R.drawable.ic_laptop_windows_black_10dp, 38))
        listDM.add(DanhMuc("Phụ kiện", R.drawable.ic_headset_black_10dp, 39))
        listDM.add(DanhMuc("Đồng hồ", R.drawable.ic_watch_black_10dp, 40))
        listDM.add(DanhMuc("Khuyễn mãi", R.drawable.ic_new_releases_black_10dp, 1))
        val layoutManager = androidx.recyclerview.widget.GridLayoutManager(
            applicationContext,
            1,
            androidx.recyclerview.widget.GridLayoutManager.HORIZONTAL,
            false
        )
        val adapter = ListDanhMucAdapter(this, listDM, object : ClickItemList {
            override fun onClick(position: Int) {
                val intent = Intent(applicationContext, DanhMucActivity::class.java)
                intent.putExtra("danhmuc", listDM.get(position))
                startActivity(intent)
            }
        })
        list_danhmuc.layoutManager = layoutManager
        list_danhmuc.adapter = adapter
        list_danhmuc.setHasFixedSize(true)
        list_danhmuc.isFocusable = false
        list_danhmuc.isNestedScrollingEnabled = false
    }

    fun getAccount() {
        nameAcc = getSharedPreferences("id", Context.MODE_PRIVATE).getString("curUser", "")
        val tendn = getSharedPreferences("id", Context.MODE_PRIVATE).getString("tendn", "")
        if (nameAcc!!.length != 0) {
            isLoggedIn = true
            presenterLogicLayThongTinNV = PresenterLogicLayThongTinNV(this)
            presenterLogicLayThongTinNV!!.LayThongTinNhanVien(tendn)
        } else {
            isLoggedIn = false
        }
    }

    override fun HienThiThongTin(nhanVien: NhanVien) {
        val sharedPreference =  applicationContext!!.getSharedPreferences("id",Context.MODE_PRIVATE)
        val editor = sharedPreference.edit()
        editor.putString("manv",nhanVien.mMaNV.toString())
        editor.commit()
    }

    fun toDangNhap() {
        val intent = Intent(this, DangNhapDangKi::class.java)
        startActivity(intent)
    }

    fun requireDangNhap() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Vui lòng đăng nhập")
        builder.setMessage("Hãy đăng nhập để tiếp tục sử dụng chức năng")
        builder.setPositiveButton("Đăng nhập", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                toDangNhap()
            }
        })
        builder.setNegativeButton("Hủy", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                dialog!!.dismiss()
            }
        })
        val dialog = builder.create()
        dialog.show()
    }

    fun dangXuat() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setTitle("Đăng xuất")
        builder.setMessage("Bạn muốn đăng xuất khỏi tài khoản này")
        builder.setPositiveButton("Đồng ý", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                if (AccessToken.getCurrentAccessToken() != null)
                    LoginManager.getInstance().logOut()
                isLoggedIn = false
                getSharedPreferences("id", Context.MODE_PRIVATE).edit().putString("curUser", "").commit()
                Toast.makeText(applicationContext, "Đăng xuất thành công", Toast.LENGTH_SHORT).show()
                recreate()
            }
        })
        builder.setNegativeButton("Hủy", object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                dialog!!.dismiss()
            }
        })
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    override fun HienThiQuangCao(arrayList: ArrayList<QuangCao>) {
        getSlider(arrayList)
    }

    override fun onStart() {
        super.onStart()
        Log.d("activiaa", "start")
    }

    override fun onPause() {
        super.onPause()
        Log.d("activiaa", "pause")
    }

    override fun onResume() {
        super.onResume()
        Log.d("activiaa", "resume")
    }

    override fun onRestart() {
        super.onRestart()
        onPrepareDrawerMenu(navigation_view.menu)
        sizeListBuy = 0
        listBuy = presenterLogicGioHang!!.getAll() as ArrayList<SanPhamGioHang>
        for (i in 0 until listBuy.size) {
            sizeListBuy += listBuy.get(i).mSoLuongMua
            Log.d("activiaa", "restart")
        }
        if (listBuy.size == 0) {
            txtSl!!.visibility = View.INVISIBLE
        } else {
            txtSl!!.visibility = View.VISIBLE
            txtSl!!.setText(sizeListBuy.toString())
        }
    }

}

//    override fun onOffsetChanged(p0: AppBarLayout?, p1: Int) {
//        if(collapsing_toolbar.height + p1 <= 1.5 * ViewCompat.getMinimumHeight(collapsing_toolbar)){
//            ln_search.alpha = 0F
//        }else{
//            ln_search.alpha = 1F
//        }
//    }


//    override fun HienThiDanhSachMenu(listLoaiSanPham: ArrayList<LoaiSanPham>) {
//        val expandAdapter: ExpandableListAdapter = ExpandAdapter(applicationContext, listLoaiSanPham)
//        expand.setAdapter(expandAdapter)
//        checked = 1
//        super.HienThiDanhSachMenu(listLoaiSanPham)
//    }
//
//
//    fun getViewPager() {
//        val fragmentManager = supportFragmentManager
//        val viewPagerAdapter = ViewPagerAdapter(fragmentManager)
//        viewpager.adapter = viewPagerAdapter
//        tablayout.setupWithViewPager(viewpager)
//        viewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tablayout))
//        tablayout.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(viewpager))
//    }


