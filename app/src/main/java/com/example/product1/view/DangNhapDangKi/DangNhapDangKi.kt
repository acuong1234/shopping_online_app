package com.example.product1.view.DangNhapDangKi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import com.example.product1.R
import com.example.product1.adapter.PagerAdapter
import com.example.product1.view.TrangChu.TrangChu
import com.facebook.CallbackManager
import kotlinx.android.synthetic.main.activity_dang_nhap_dang_ki.*

class DangNhapDangKi : AppCompatActivity(),TruyenDuLieuDangNhap {
    var callbackManager : CallbackManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dang_nhap_dang_ki)
        showViewPager()
    }

    fun showViewPager(){
        val pagerAdapter:PagerAdapter= PagerAdapter(supportFragmentManager)
        viewpager_dangnhapdangki.adapter = pagerAdapter
        viewpager_dangnhapdangki.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tablayout_dangnhapdangki))
        tablayout_dangnhapdangki.setupWithViewPager(viewpager_dangnhapdangki)
        tablayout_dangnhapdangki.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(viewpager_dangnhapdangki))
    }

    override fun truyenDuLieu(i: Int) {
        super.truyenDuLieu(i)
        if(i==1){
            var intent = Intent(this,TrangChu::class.java)
            startActivity(intent)
        }
    }
}
