package com.example.product1.view.DonHang

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.product1.R
import com.example.product1.adapter.ClickItemList
import com.example.product1.adapter.ListDonHangApdater
import com.example.product1.model.ObjectClass.DonHang
import com.example.product1.presenter.DonHang.PresenterLogicLayDanhSachDH
import kotlinx.android.synthetic.main.activity_danh_sach_don_hang.*

class DanhSachDonHang : AppCompatActivity(),ViewLayDanhSachDH {

    var presenterLogicLayDanhSachDH: PresenterLogicLayDanhSachDH? = null
    var mDialogWait :AlertDialog? = null
    var listDonHang = ArrayList<DonHang>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_danh_sach_don_hang)

        Log.d("testAsycn","create")

        val manv = getSharedPreferences("id", Context.MODE_PRIVATE).getString("manv", "")


        presenterLogicLayDanhSachDH = PresenterLogicLayDanhSachDH(this)
        presenterLogicLayDanhSachDH!!.LayDanhSachDH(Integer.parseInt(manv))
    }

    fun displayListDonHang(listDH : ArrayList<DonHang>){
        listDonHang = listDH
        val layoutManager = LinearLayoutManager(this)
        val adaper = ListDonHangApdater(this,listDonHang,object: ClickItemList{
            override fun onClick(position: Int) {
                val intent = Intent(applicationContext,ChiTietDonHang::class.java)
                intent.putExtra("donhang", listDonHang.get(position))
                startActivity(intent)
            }
        })
        list_don_hang.layoutManager = layoutManager
        list_don_hang.adapter = adaper
        list_don_hang.setHasFixedSize(true)
        list_don_hang.isFocusable = false
        list_don_hang.isNestedScrollingEnabled = true
        list_don_hang.adapter!!.notifyDataSetChanged()
    }

    override fun HienThiDanhSachDH(listDH: ArrayList<DonHang>) {
//        progress_bar.visibility = View.VISIBLE
        displayListDonHang(listDH)

    }
}
