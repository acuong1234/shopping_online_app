package com.example.product1.view.DangNhapDangKi.Fragment

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.product1.R
import com.example.product1.model.DangKy.ModelDangKy
import com.example.product1.model.DangNhap.ModelDangNhap
import com.example.product1.model.ObjectClass.NhanVien
import com.example.product1.presenter.DangKy.PresenterLogicDangKy
import com.example.product1.presenter.DangNhap.PresenterLogicDangNhap
import com.example.product1.view.DangNhapDangKi.TruyenDuLieuDangNhap
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import org.json.JSONObject
import java.util.*





@Suppress("DEPRECATION")
class DangNhap : androidx.fragment.app.Fragment(){

    var buttonLoginFB : Button? = null
    var buttonLoginGG : Button? = null
    var callbackManager : CallbackManager? = null
    var truyenDuLieuDangNhap : TruyenDuLieuDangNhap? = null
    var nameAcc: String =""
    var linkAcc: String =""
    var presenterLogicDangKy: PresenterLogicDangKy? = null
    var presenterLieuDangNhap : PresenterLogicDangNhap? = null
    var emailInput : EditText? = null
    var passInput: EditText? = null
    var buttonDangNhap : Button? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_dangnhap,container,false)
        buttonLoginFB = view.findViewById(R.id.btn_dangnhap_fb)
        buttonLoginGG = view.findViewById(R.id.btn_dangnhap_gg)
        emailInput = view.findViewById(R.id.edt_email)
        passInput = view.findViewById(R.id.edt_pass)
        buttonDangNhap = view.findViewById(R.id.btn_dangnhap)
        truyenDuLieuDangNhap = activity as TruyenDuLieuDangNhap

        FacebookSdk.sdkInitialize(context!!.applicationContext)
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    Log.d("FacebookDN","Thành công")

                    val accessToken= AccessToken.getCurrentAccessToken()
                    val request: GraphRequest =
                        GraphRequest.newMeRequest(accessToken, object : GraphRequest.GraphJSONObjectCallback {
                            override fun onCompleted(`object`: JSONObject?, response: GraphResponse?) {
                                nameAcc = `object`!!.getString("name")

                                dangki(nameAcc)

                                truyenDuLieuDangNhap!!.truyenDuLieu(1)
                            }
                        })
                    val parameters = Bundle()
                    parameters.putString("fields", "name")
                    request.setParameters(parameters)
                    request.executeAsync()
                }

                override fun onCancel() {
                    Log.d("FacebookDN","Error")
                }

                override fun onError(exception: FacebookException) {
                    Log.d("FacebookDN","Thất bại")
                }
            })

        buttonLoginFB!!.setOnClickListener {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
        }

        buttonLoginGG!!.setOnClickListener {
            val builder : AlertDialog.Builder = AlertDialog.Builder(context!!)
            builder.setTitle("Thông báo")
            builder.setMessage("API Google+ không còn hỗ trợ")
            builder.setPositiveButton("OK",object : DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    dialog!!.dismiss()
                }
            })
            val dialog = builder.create()
            dialog.show()
        }

        buttonDangNhap!!.setOnClickListener {
            dangnhap()
        }

        return view
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode,resultCode,data)
    }

    fun dangki(name:String){
        val nhanVien = NhanVien()

        nhanVien.mTenNV = name
        nhanVien.mTenDangNhap = name+"xax"
        nhanVien.mMatKhau = ""
        nhanVien.mMaLoaiNV = 2
        nhanVien.mEmailDocQuyen = "true"
        nhanVien.mFromFb = 1

        presenterLogicDangKy = PresenterLogicDangKy(ModelDangKy())
        presenterLogicDangKy!!.ThucHienDangKy(nhanVien)


        val sharedPreference =  context!!.getSharedPreferences("id",Context.MODE_PRIVATE)
        val editor = sharedPreference.edit()
        editor.putString("curUser",name)
        editor.putString("tendn",name+"xax")
        editor.commit()
    }

    fun dangnhap(){
        presenterLieuDangNhap = PresenterLogicDangNhap(ModelDangNhap())
        val tennv = presenterLieuDangNhap!!.KiemTraDangNhap(emailInput!!.text.toString(),passInput!!.text.toString())
        if(tennv!!.length!=0){
            Toast.makeText(context,"Đăng nhập thành công",Toast.LENGTH_SHORT).show()
            val sharedPreference =  context!!.getSharedPreferences("id",Context.MODE_PRIVATE)
            val editor = sharedPreference.edit()
            editor.putString("curUser",tennv)
            editor.putString("tendn",emailInput!!.text.toString())
            editor.commit()
            truyenDuLieuDangNhap!!.truyenDuLieu(1)
        }else{
            Toast.makeText(context,"Tài khoản hoặc mật khẩu không chính xác",Toast.LENGTH_SHORT).show()
        }
    }
}