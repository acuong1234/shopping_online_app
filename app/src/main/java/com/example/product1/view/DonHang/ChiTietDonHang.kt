package com.example.product1.view.DonHang

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.product1.R
import com.example.product1.adapter.ListChiTietDHAdapter
import com.example.product1.model.ObjectClass.DonHang
import com.example.product1.model.ObjectClass.ToVNMoney
import kotlinx.android.synthetic.main.activity_chi_tiet_don_hang.*
import kotlinx.android.synthetic.main.activity_gio_hang.*

class ChiTietDonHang : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chi_tiet_don_hang)

        val intent = intent
        val donhang = intent.getSerializableExtra("donhang") as DonHang

        val adapter = ListChiTietDHAdapter(applicationContext,donhang.mChiTietDonHang)
        val layoutManager = GridLayoutManager(this,1)
        list_item_donhang_detail.adapter = adapter
        list_item_donhang_detail.layoutManager = layoutManager as RecyclerView.LayoutManager?
        list_item_donhang_detail.setHasFixedSize(true)
        txt_total_price_detail.setText(ToVNMoney().toVNMoneyFunction(donhang.mTongTien))
        txt_dia_chi_detail.setText(donhang.mDiaChiNhan)
        txt_sdt_detail.append(donhang.mSoDTNhan)
        when(Integer.parseInt(donhang.mTrangThai)){
            0 -> txt_status_detail.append("Đã hoàn thành")
            1 -> txt_status_detail.append("Đang giao hàng")
            2 -> txt_status_detail.append("Đã xác nhận đơn hàng")
            3 -> txt_status_detail.append("Đang đợi xác nhận")
        }
    }
}
