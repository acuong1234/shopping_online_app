package com.example.product1.view.DangNhapDangKi.Fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.product1.R
import com.example.product1.model.DangKy.ModelDangKy
import com.example.product1.model.ObjectClass.NhanVien
import com.example.product1.presenter.DangKy.PresenterLogicDangKy
import com.example.product1.view.DangNhapDangKi.TruyenDuLieuDangNhap

class DangKi : androidx.fragment.app.Fragment(){

    var presenterLogicDangKy: PresenterLogicDangKy? = null
    var edtName: EditText? = null
    var edtId: EditText? = null
    var edtPass: EditText? = null
    var edtConfirmPass: EditText? = null
    var btnDangKy: Button? = null
    var switchEmail: Switch? = null
    var truyenDuLieuDangNhap : TruyenDuLieuDangNhap? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view: View = inflater.inflate(R.layout.fragment_dangki, container, false)
        edtName = view.findViewById(R.id.edtinput_name)
        edtId = view.findViewById(R.id.edtinput_id)
        edtPass = view.findViewById(R.id.edtinput_pass)
        edtConfirmPass = view.findViewById(R.id.edtinput_confirmpass)
        btnDangKy = view.findViewById(R.id.btn_dangky)
        switchEmail = view.findViewById(R.id.switch_check_email)
        truyenDuLieuDangNhap = activity as TruyenDuLieuDangNhap

        presenterLogicDangKy = PresenterLogicDangKy(ModelDangKy())

        btnDangKy!!.setOnClickListener {
            DangKy()
        }

        return view
    }

    fun DangKy() {
        var emaildq : String = ""
        val nameIn = edtName!!.text.toString()
        val idIn = edtId!!.text.toString()
        val passIn = edtPass!!.text.toString()
        val conpassIn = edtConfirmPass!!.text.toString()

        if (nameIn.length == 0 || idIn.length == 0 || passIn.length == 0 || conpassIn.length == 0) {
            Toast.makeText(context, "Nhập thiếu thông tin", Toast.LENGTH_SHORT).show()
        }else if(!passIn.equals(conpassIn,true)){
            Toast.makeText(context, "Mật khẩu không khớp nhau", Toast.LENGTH_SHORT).show()
        }else{
            val nhanVien = NhanVien()

            nhanVien.mTenNV = edtName!!.text.toString()
            nhanVien.mTenDangNhap = edtId!!.text.toString()
            nhanVien.mMatKhau = edtPass!!.text.toString()
            nhanVien.mMaLoaiNV = 2
            nhanVien.mFromFb = 0

            if(switchEmail!!.isChecked){
                nhanVien.mEmailDocQuyen = "true"
            }else{
                nhanVien.mEmailDocQuyen = "false"
            }

            if(presenterLogicDangKy!!.ThucHienDangKy(nhanVien)){
                Toast.makeText(context, "Đăng kí thành công", Toast.LENGTH_SHORT).show()

                val sharedPreference =  context!!.getSharedPreferences("id", Context.MODE_PRIVATE)
                val editor = sharedPreference.edit()
                editor.putString("curUser",nhanVien.mTenNV)
                editor.putString("tendn",nhanVien.mTenDangNhap)
                editor.commit()

                truyenDuLieuDangNhap!!.truyenDuLieu(1)
            }else{
                Toast.makeText(context, "Tên đăng nhập đã được sử dụng", Toast.LENGTH_SHORT).show()
            }


        }



    }

}