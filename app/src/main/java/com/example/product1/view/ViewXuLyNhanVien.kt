package com.example.product1.view

import com.example.product1.model.ObjectClass.NhanVien

interface ViewXuLyNhanVien {
    fun HienThiThongTin(nhanVien: NhanVien)
}