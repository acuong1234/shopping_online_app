package com.example.product1.presenter.TrangChu.XuLyMenu

import com.example.product1.model.ObjectClass.LoaiSanPham

interface IPresenterXuLyMenu {
    fun layDanhSachMenu()
}