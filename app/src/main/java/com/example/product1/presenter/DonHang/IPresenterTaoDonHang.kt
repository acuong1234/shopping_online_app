package com.example.product1.presenter.DonHang

import com.example.product1.model.ObjectClass.DonHang

interface IPresenterTaoDonHang {
    fun TaoDonHang(donHang: DonHang)
}