package com.example.product1.presenter.DangKy

import android.util.Log
import com.example.product1.model.DangKy.ModelDangKy
import com.example.product1.model.ObjectClass.NhanVien

class PresenterLogicDangKy(var modelDangKy: ModelDangKy) : IPresenterDangKy {
    override fun ThucHienDangKy(nhanVien: NhanVien) :Boolean {
        val check = modelDangKy.DangKyThanhVien(nhanVien)
        return check
    }
}