package com.example.product1.presenter.TrangChu.XuLyMenu

import android.util.Log
import com.example.product1.R
import com.example.product1.connectinternet.DownloadJson
import com.example.product1.model.ObjectClass.Link
import com.example.product1.model.ObjectClass.LoaiSanPham
import com.example.product1.model.TrangChu.XuLyMenu.XuLyJsonMenu
import com.example.product1.view.TrangChu.ViewXuLyMenu
import java.util.ArrayList
import java.util.HashMap
import java.util.concurrent.ExecutionException

class PresenterLogicXuLyMenu(var viewXuLyMenu: ViewXuLyMenu) : IPresenterXuLyMenu {
    override fun layDanhSachMenu() {
        var loaiSanPhamList: List<LoaiSanPham>
        var dataJSON = ""
        val attrs = ArrayList<HashMap<String, String>>()

        //Lấy bằng phương thức get
//        val Link = "http://192.168.0.103:8080/weblazada/loaisanpham.php?maloaicha=0"


        // end phương thức get

        //Lấy bằng phương thức post
//        val Link = "http://192.168.0.21:8080/weblazada/loaisanpham.php"
        val link = Link()
        val duongdan = link.link
        Log.d("Test",duongdan)

        val hsHam = HashMap<String, String>()
        hsHam["ham"] = "LayDanhSachMenu"

        val hsMaLoaiCha = HashMap<String, String>()
        hsMaLoaiCha["maloaicha"] = "0"

        attrs.add(hsMaLoaiCha)
        attrs.add(hsHam)

        val downloadJSON = DownloadJson(duongdan, attrs)
        //end phương thức post
        downloadJSON.execute()

        try {
            dataJSON = downloadJSON.get()
            val xuLyJSONMenu = XuLyJsonMenu()
            loaiSanPhamList = xuLyJSONMenu.parseJsonMenu(dataJSON)
            viewXuLyMenu.HienThiDanhSachMenu(loaiSanPhamList)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        } catch (e: ExecutionException) {
            e.printStackTrace()
        }
    }
}