package com.example.product1.presenter.SanPham

import com.example.product1.connectinternet.DownloadJson
import com.example.product1.model.ObjectClass.Link
import com.example.product1.model.ObjectClass.SanPham
import com.example.product1.model.SanPham.XuLySanPhamTheoDanhMuc
import com.example.product1.view.TrangChu.ViewSPTheoDanhMuc
import java.util.concurrent.ExecutionException

class PresenterLogicSanPhamTheoDanhMuc(var viewSPTheoDanhMuc: ViewSPTheoDanhMuc) :IPresenterSanPhamTheoDanhMuc {
    override fun LayDanhSachSPTheoDanhMuc(maDanhMuc:Int) {
        var listSanPham : List<SanPham>
        var dataJSON = ""
        val attrs = ArrayList<HashMap<String,String>>()

        val link = Link()
        val duongdan = link.link

        val hsHam = java.util.HashMap<String, String>()
        hsHam["ham"] = "LayDanhSachSPTheoDanhMuc"

        val hsMaLoaiSP = java.util.HashMap<String, String>()
        hsMaLoaiSP["maloaisp"] = maDanhMuc.toString()

        attrs.add(hsMaLoaiSP)
        attrs.add(hsHam)

        val downloadJSON = DownloadJson(duongdan, attrs)
        downloadJSON.execute()

        try {
            dataJSON = downloadJSON.get()
            val xuLyNoiBat = XuLySanPhamTheoDanhMuc()
            listSanPham = xuLyNoiBat.parseJsonSanPham(dataJSON)
            viewSPTheoDanhMuc.HienThiSPTheoDanhMuc(listSanPham)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        } catch (e: ExecutionException) {
            e.printStackTrace()
        }
    }
}