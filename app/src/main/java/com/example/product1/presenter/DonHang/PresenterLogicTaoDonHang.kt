package com.example.product1.presenter.DonHang

import com.example.product1.model.DonHang.ModelTaoDonHang
import com.example.product1.model.ObjectClass.DonHang
import com.example.product1.view.GioHang.ViewTaoDonHang

class PresenterLogicTaoDonHang(val viewTaoDonHang: ViewTaoDonHang) : IPresenterTaoDonHang {
    override fun TaoDonHang(donHang: DonHang) {
        val modelTaoDonHang = ModelTaoDonHang()
        if(modelTaoDonHang.TaoDonHang(donHang)){
            viewTaoDonHang.TaoThanhCong()
        }else{
            viewTaoDonHang.TaoThatBai()
        }
    }
}