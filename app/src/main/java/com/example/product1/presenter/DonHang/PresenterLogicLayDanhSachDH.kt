package com.example.product1.presenter.DonHang

import android.os.AsyncTask
import android.util.Log
import com.example.product1.model.DonHang.ModelLayDanhSachDonHang
import com.example.product1.model.ObjectClass.DonHang
import com.example.product1.view.DonHang.ViewLayDanhSachDH

class PresenterLogicLayDanhSachDH(var viewLayDanhSachDH: ViewLayDanhSachDH) : IPresenterLayDanhSachDH {
    override fun LayDanhSachDH(manv: Int) {
        val modelLayDanhSachDonHang = ModelLayDanhSachDonHang()
        viewLayDanhSachDH.HienThiDanhSachDH(modelLayDanhSachDonHang.LayDanhSachDonHang(manv))
//        Log.d("testAsycn",listDH.size.toString())
//        viewLayDanhSachDH.HienThiDanhSachDH(listDH)
    }
}