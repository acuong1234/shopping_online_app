package com.example.product1.presenter.NhanVien

import com.example.product1.model.NhanVien.ModelLayThongTinNhanVien
import com.example.product1.view.ViewXuLyNhanVien

class PresenterLogicLayThongTinNV(val viewXuLyNhanVien: ViewXuLyNhanVien) : IPresenterLayThongTinNV {
    override fun LayThongTinNhanVien(tendn: String) {
        val modelLayThongTinNhanVien = ModelLayThongTinNhanVien()
        viewXuLyNhanVien.HienThiThongTin(modelLayThongTinNhanVien.LayThongTinNV(tendn))
    }

}