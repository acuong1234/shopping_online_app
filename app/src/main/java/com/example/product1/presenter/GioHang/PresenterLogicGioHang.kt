package com.example.product1.presenter.GioHang

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.product1.model.GioHang.SanPhamGioHangDao
import com.example.product1.model.ObjectClass.SanPhamGioHang

class PresenterLogicGioHang(var sanPhamGioHangDao: SanPhamGioHangDao) : IPresenterGioHang {
    override fun deletaAll() {
        DeleteAllTask(sanPhamGioHangDao).execute()
    }

    override fun insert(sanPhamGioHang: SanPhamGioHang) {
        InsertTask(sanPhamGioHangDao).execute(sanPhamGioHang)
    }

    override fun delete(sanPhamGioHang: SanPhamGioHang) {
        DeleteTask(sanPhamGioHangDao).execute(sanPhamGioHang)
    }

    override fun update(sanPhamGioHang: SanPhamGioHang) {
        UpdateTask(sanPhamGioHangDao).execute(sanPhamGioHang)
    }

    override fun getAll(): List<SanPhamGioHang> {
        return GetAllTask(sanPhamGioHangDao).execute().get()
    }

    private class GetAllTask internal
    constructor(private var dao: SanPhamGioHangDao) : AsyncTask<Void, Void, List<SanPhamGioHang>>() {
        override fun doInBackground(vararg params: Void?): List<SanPhamGioHang> {
            return dao.getAll()
        }

    }

    private class DeleteTask internal
    constructor(private var dao: SanPhamGioHangDao) : AsyncTask<SanPhamGioHang, Void, Boolean>() {
        override fun doInBackground(vararg params: SanPhamGioHang): Boolean? {
            dao.delete(params[0])
            return true
        }
    }

    private class DeleteAllTask internal
    constructor(private var dao: SanPhamGioHangDao) : AsyncTask<Void, Void, Boolean>() {
        override fun doInBackground(vararg params: Void?): Boolean {
            dao.deletAll()
            return true
        }

    }

    private class InsertTask internal
    constructor(private var dao: SanPhamGioHangDao) : AsyncTask<SanPhamGioHang, Void, Boolean>() {
        override fun doInBackground(vararg params: SanPhamGioHang): Boolean? {
            dao.insert(params[0])
            return true
        }
    }

    private class UpdateTask internal
    constructor(private var dao: SanPhamGioHangDao) : AsyncTask<SanPhamGioHang, Void, Boolean>() {
        override fun doInBackground(vararg params: SanPhamGioHang): Boolean? {
            dao.Update(params[0])
            return true
        }
    }
}