package com.example.product1.presenter.GioHang

import androidx.lifecycle.LiveData
import com.example.product1.model.ObjectClass.SanPhamGioHang

interface IPresenterGioHang {
    fun insert(sanPhamGioHang: SanPhamGioHang)
    fun delete(sanPhamGioHang: SanPhamGioHang)
    fun update(sanPhamGioHang: SanPhamGioHang)
    fun getAll() : List<SanPhamGioHang>
    fun deletaAll()
}