package com.example.product1.presenter.DangKy

import com.example.product1.model.ObjectClass.NhanVien


interface IPresenterDangKy {
    fun ThucHienDangKy(nhanVien: NhanVien) : Boolean
}