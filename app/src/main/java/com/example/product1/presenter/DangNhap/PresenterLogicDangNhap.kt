package com.example.product1.presenter.DangNhap

import com.example.product1.model.DangNhap.ModelDangNhap

class PresenterLogicDangNhap(var modelDangNhap: ModelDangNhap) : IPresenterDangNhap {
    override fun KiemTraDangNhap(tendangnhap: String, matkhau: String): String {
        val tennv : String = modelDangNhap.DangNhap(tendangnhap,matkhau)
        return tennv
    }
}