package com.example.product1.presenter.TrangChu.XuLyQC

import android.util.Log
import com.example.product1.connectinternet.DownloadJson
import com.example.product1.model.ObjectClass.Link
import com.example.product1.model.ObjectClass.QuangCao
import com.example.product1.model.TrangChu.XuLyQuangCao.XuLyQuangCao
import com.example.product1.view.TrangChu.ViewQuangCao
import java.util.concurrent.ExecutionException

class PresenterLogicQC(var viewXuLyQC: ViewQuangCao):IPresenterXuLyQC {
    override fun layDanhSachQC() {
        var listQC = ArrayList<QuangCao>()
        var dataJSON = ""
        var attrs = ArrayList<HashMap<String,String>>()
        val link = Link()
        var duongdan = link.link
        Log.d("testJson",duongdan)

        val hsHam = HashMap<String,String>()
        hsHam["ham"] = "LayDanhSachQC"

        val hsHieuLuc = HashMap<String,String>()
        hsHieuLuc["hieuluc"] = "1"

        attrs.add(hsHam)
        attrs.add(hsHieuLuc)

        val downloadJson = DownloadJson(duongdan,attrs)
        downloadJson.execute()

        try {
            dataJSON = downloadJson.get()
            Log.d("testJson",dataJSON)
            val xuLyQuangCao = XuLyQuangCao()
            listQC = xuLyQuangCao.parseJsonQC(dataJSON)
            viewXuLyQC.HienThiQuangCao(listQC)
        }catch (e: InterruptedException){
            e.printStackTrace()
        }catch (e: ExecutionException){
            e.printStackTrace()
        }
    }
}