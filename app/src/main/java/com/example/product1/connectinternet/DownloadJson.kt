package com.example.product1.connectinternet

import android.net.Uri
import android.os.AsyncTask
import java.io.*
import java.lang.StringBuilder
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.ProtocolException
import java.net.URL

class DownloadJson(var mDuongDan: String, var mAttrs: List<HashMap<String, String>>?) :
    AsyncTask<String, Void, String>() {
    var mDuLieu = StringBuilder()
    override fun doInBackground(vararg params: String): String {
        var dulieu:String=""
        try {
            val url: URL = URL(mDuongDan)
            val httpURLConnection: HttpURLConnection = url.openConnection() as HttpURLConnection
            if (mAttrs != null) {
                dulieu = methodPost(httpURLConnection)
            } else {
                dulieu = methodGet(httpURLConnection)
            }

        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return dulieu
    }

    private fun methodGet(httpURLConnection: HttpURLConnection): String {
        val inputStream:InputStream = httpURLConnection.inputStream
        val streamReader: InputStreamReader = InputStreamReader(inputStream)
        val bufferedReader: BufferedReader = BufferedReader(streamReader as Reader?)

        try {
            httpURLConnection.connect()
            while (bufferedReader.readLine() != null) {
                mDuLieu.append(bufferedReader.readLine())
            }
            bufferedReader.close()
            streamReader.close()
            inputStream.close()
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return mDuLieu.toString()
    }

    private fun methodPost(httpURLConnection: HttpURLConnection): String {
//        var dulieu: String = ""
//        var key: String = ""
//        var value: String = ""
//        try {
//            httpURLConnection.requestMethod = "POST"
//            httpURLConnection.doOutput = true
//            httpURLConnection.doInput = true
//
//            var builder = Uri.Builder()
//            val count:Int = mAttrs!!.size
////            for (i in 1 until count step 1) {
////                for (values: Map.Entry<String, String> in mAttrs!!.get(i).) {
////                    key = values.key
////                    value = values.value
////                }
////                builder.appendQueryParameter(key, value)
////            }
//
//            for (i in 0 until count) {
//                for ((key1, value1) in mAttrs!!.get(i)) {
//                    key = key1
//                    value = value1
//                }
//                builder.appendQueryParameter(key, value)
//            }
//
//            val query = builder.build().encodedQuery
//
//            val outPutStream: OutputStream = httpURLConnection.outputStream
//            val streamWriter: OutputStreamWriter = OutputStreamWriter(outPutStream)
//            val bufferWriter: BufferedWriter = BufferedWriter(streamWriter)
//
//            bufferWriter.write(query)
//            bufferWriter.flush()
//            bufferWriter.close()
//
//            streamWriter.close()
//            outPutStream.close()
//
//            dulieu = methodGet(httpURLConnection)
//        } catch (e: ProtocolException) {
//            e.printStackTrace()
//        }
//        return dulieu

        var data = ""
        var key = ""
        var value = ""

        try {
            httpURLConnection.requestMethod = "POST"
            httpURLConnection.doOutput = true
            httpURLConnection.doInput = true

            val builder = Uri.Builder()

            val count = mAttrs!!.size
            for (i in 0 until count) {

                for (values in mAttrs!!.get(i).entries) {
                    key = values.key
                    value = values.value
                }
                builder.appendQueryParameter(key, value)
            }
            val query = builder.build().encodedQuery

            val outputStream = httpURLConnection.outputStream
            val streamWriter = OutputStreamWriter(outputStream)
            val writer = BufferedWriter(streamWriter)

            writer.write(query)
            writer.flush()
            writer.close()
            streamWriter.close()
            outputStream.close()

            data = methodGet(httpURLConnection)

        } catch (e: ProtocolException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return data
    }
}