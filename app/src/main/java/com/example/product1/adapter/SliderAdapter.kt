package com.example.product1.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.product1.R
import com.example.product1.model.ObjectClass.QuangCao
import com.squareup.picasso.Picasso



class SliderAdapter(var context: Context,var arrayList: ArrayList<QuangCao>, var clickItemList: ClickItemList): androidx.viewpager.widget.PagerAdapter() {
    override fun getCount(): Int {
        return arrayList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layoutInflater : LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view : View = layoutInflater.inflate(R.layout.image_slider_layout_item,null)
        val img = view.findViewById<ImageView>(R.id.iv_auto_image_slider)
        val photo = arrayList.get(position).mAnhQC
        Picasso.get().load(photo).fit().into(img)
        view.setOnClickListener {
            clickItemList.onClick(position)
        }

        val vp : androidx.viewpager.widget.ViewPager = container as androidx.viewpager.widget.ViewPager
        vp.addView(view,0)

        return view
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as androidx.viewpager.widget.ViewPager
        val view = `object` as View
        vp.removeView(view)
    }
}





