package com.example.product1.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.room.Room
import com.example.product1.R
import com.example.product1.model.GioHang.ModelGioHangRoomDb
import com.example.product1.model.ObjectClass.SanPhamGioHang
import com.example.product1.model.ObjectClass.ToVNMoney
import com.example.product1.presenter.GioHang.PresenterLogicGioHang
import com.squareup.picasso.Picasso

class ListGioHangAdapter(var context: Context, var arrayList: ArrayList<SanPhamGioHang>,var presenterLogicGioHang: PresenterLogicGioHang, var dataChangeGioHang: DataChangeGioHang) :
    androidx.recyclerview.widget.RecyclerView.Adapter<ListGioHangAdapter.ListGioHangAdapterVH>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ListGioHangAdapterVH {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_list_gio_hang, p0, false)
        return ListGioHangAdapterVH(view)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(p0: ListGioHangAdapterVH, p1: Int) {

        val photo = arrayList.get(p1).mAnhNho
        Picasso.get().load(photo).into(p0.img)
        p0.txtName.setText(arrayList.get(p1).mTenSP)
        p0.edtNum.isFocusable = false
        p0.edtNum.setText((arrayList.get(p1).mSoLuongMua).toString())
        p0.txtPrice.setText(ToVNMoney().toVNMoneyFunction(arrayList.get(p1).mGia * arrayList.get(p1).mSoLuongMua))

        p0.btnAdd.setOnClickListener {
            arrayList.get(p1).mSoLuongMua++
            p0.edtNum.setText((arrayList.get(p1).mSoLuongMua).toString())
            p0.txtPrice.setText(ToVNMoney().toVNMoneyFunction(arrayList.get(p1).mGia * arrayList.get(p1).mSoLuongMua))
            dataChangeGioHang.dataIncrease(arrayList.get(p1).mGia,p1)
            presenterLogicGioHang.update(arrayList.get(p1))
        }

        p0.btnRemove.setOnClickListener {
            if (arrayList.get(p1).mSoLuongMua > 1) {
                arrayList.get(p1).mSoLuongMua--
                presenterLogicGioHang.update(arrayList.get(p1))
                p0.edtNum.setText((arrayList.get(p1).mSoLuongMua).toString())
                p0.txtPrice.setText(ToVNMoney().toVNMoneyFunction(arrayList.get(p1).mGia * arrayList.get(p1).mSoLuongMua))
                dataChangeGioHang.dataDecrease(arrayList.get(p1).mGia,p1)
            }else if (arrayList.get(p1).mSoLuongMua == 1) {
                presenterLogicGioHang.delete(arrayList.get(p1))
                dataChangeGioHang.dataDecrease(arrayList.get(p1).mGia,p1)
                arrayList.removeAt(p1)
                notifyDataSetChanged()
            }
        }
    }


    class ListGioHangAdapterVH(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        val img: ImageView = itemView.findViewById(R.id.img_item_giohang)
        val txtName: TextView = itemView.findViewById(R.id.txt_name_item_giohang)
        val btnAdd: ImageButton = itemView.findViewById(R.id.btn_add_item_giohang)
        val btnRemove: ImageButton = itemView.findViewById(R.id.btn_remove_item_giohang)
        val edtNum: EditText = itemView.findViewById(R.id.edt_soluong_item_giohang)
        val txtPrice: TextView = itemView.findViewById(R.id.txt_price_item_giohang)
    }
}