package com.example.product1.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.product1.view.DangNhapDangKi.Fragment.DangKi
import com.example.product1.view.DangNhapDangKi.Fragment.DangNhap

class PagerAdapter(val fm: androidx.fragment.app.FragmentManager) : androidx.fragment.app.FragmentStatePagerAdapter(fm) {
    override fun getItem(p0: Int): androidx.fragment.app.Fragment {
        var fragment = androidx.fragment.app.Fragment()
        when (p0){
            0->fragment=DangNhap()
            1->fragment=DangKi()
        }
        return fragment
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title:String =""
        when(position){
            0->title="Đăng nhập"
            1->title="Đăng kí"
        }
        return title
    }
}