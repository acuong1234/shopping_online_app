package com.example.product1.adapter

import android.content.Context
import android.graphics.Point
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.BaseExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.ImageView
import android.widget.TextView
import com.example.product1.R
import com.example.product1.model.ObjectClass.LoaiSanPham

class ExpandAdapter(val context: Context, val listLoaiSanPham: ArrayList<LoaiSanPham>) : BaseExpandableListAdapter() {
//    init {
//        val xuLyJsonMenu:XuLyJsonMenu = XuLyJsonMenu()
//        val count:Int = listLoaiSanPham.size
//        for(i in 0 until  count-1){
//            val maloaisp = listLoaiSanPham.get(i).mMaLoaiSP
//            listLoaiSanPham.get(i).mListCon=xuLyJsonMenu.layDanhSachTheoMaLoai(maloaisp)
//        }
//    }

    override fun getGroupCount(): Int {
        return listLoaiSanPham.size
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        if(listLoaiSanPham.get(groupPosition).mListCon!!.size!=0){
            return 1
        }else{
            return 0
        }
    }

    override fun getGroup(groupPosition: Int): Any {
        return listLoaiSanPham.get(groupPosition)
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return listLoaiSanPham.get(groupPosition).mListCon!!.get(childPosition)
    }

    override fun getGroupId(groupPosition: Int): Long {
        return listLoaiSanPham.get(groupPosition).mMaLoaiSP.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return listLoaiSanPham.get(groupPosition).mListCon!!.get(childPosition).mMaLoaiSP.toLong()
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        val layoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val viewGroupCha: View = layoutInflater.inflate(R.layout.custom_layout_groupcha, parent, false)

        val txtTenLoaiSP: TextView = viewGroupCha.findViewById(R.id.txt_tenloaisp)
        val imgAddRemove: ImageView = viewGroupCha.findViewById(R.id.img_add_remove)
        val count: Int = listLoaiSanPham.get(groupPosition).mListCon!!.size

        txtTenLoaiSP.setText(listLoaiSanPham.get(groupPosition).mTenLoaiSP)
        if(isExpanded){
            viewGroupCha.setBackgroundResource(R.color.colorGray)
            imgAddRemove.setImageResource(R.drawable.ic_remove_black_24dp)
        }

        if(count == 0){
            imgAddRemove.setImageResource(android.R.color.transparent)
        }

        viewGroupCha.setOnTouchListener { v, event ->
            Log.d("loaisp",listLoaiSanPham.get(groupPosition).mTenLoaiSP+listLoaiSanPham.get(groupPosition).mMaLoaiSP)
            false
        }

        return viewGroupCha
    }

    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup?    ): View {

        val secondExpand:ExpandableListView = SecondExpand(context)
        val adapter:ExpandAdapter= ExpandAdapter(context,
            listLoaiSanPham.get(groupPosition).mListCon as ArrayList<LoaiSanPham>
        )


        secondExpand.setGroupIndicator(null)
        secondExpand.choiceMode = ExpandableListView.CHOICE_MODE_SINGLE
        secondExpand.setAdapter(adapter)
        notifyDataSetChanged()
        return secondExpand
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    class SecondExpand(context: Context?) : ExpandableListView(context) {
        override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
            val windowMenu:WindowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = windowMenu.defaultDisplay
            val size: Point = Point()
            display.getSize(size)

//            val width:Int = size.x
            val height:Int = size.y

//            val widthM:Int = MeasureSpec.makeMeasureSpec(width,MeasureSpec.AT_MOST)
            val heightM:Int = MeasureSpec.makeMeasureSpec(height,MeasureSpec.AT_MOST)

            super.onMeasure(widthMeasureSpec, heightM)
        }
    }

//    class SecondExpandAdapter(var context: Context,var listcon: ArrayList<LoaiSanPham>) : BaseExpandableListAdapter() {
//        override fun getGroupCount(): Int {
//            return listcon.size
//        }
//
//        override fun getChildrenCount(groupPosition: Int): Int {
//            return listcon.get(groupPosition).mListCon!!.size
//        }
//
//        override fun getGroup(groupPosition: Int): Any {
//            return listcon.get(groupPosition)
//        }
//
//        override fun getChild(groupPosition: Int, childPosition: Int): Any {
//            return listcon.get(groupPosition).mListCon!!.get(childPosition)
//        }
//
//        override fun getGroupId(groupPosition: Int): Long {
//            return listcon.get(groupPosition).mMaLoaiSP.toLong()
//        }
//
//        override fun getChildId(groupPosition: Int, childPosition: Int): Long {
//            return listcon.get(groupPosition).mListCon!!.get(childPosition).mMaLoaiSP.toLong()
//        }
//
//        override fun hasStableIds(): Boolean {
//            return false
//        }
//
//        override fun getGroupView(
//            groupPosition: Int,
//            isExpanded: Boolean,
//            convertView: View?,
//            parent: ViewGroup?
//        ): View {
//            val layoutInflater: LayoutInflater =
//                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//            val viewGroupCha: View = layoutInflater.inflate(R.layout.custom_layout_groupcha, parent, false)
//            val txtTenLoaiSP: TextView = viewGroupCha.findViewById(R.id.txt_tenloaisp)
//            txtTenLoaiSP.setText(listcon.get(groupPosition).mTenLoaiSP)
//
//            return viewGroupCha
//        }
//
//        override fun getChildView(
//            groupPosition: Int,
//            childPosition: Int,
//            isLastChild: Boolean,
//            convertView: View?,
//            parent: ViewGroup?
//        ): View {
//            val layoutInflater: LayoutInflater =
//                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//            val viewGroupCon: View = layoutInflater.inflate(R.layout.custom_layout_groupcha, parent, false)
//            val txtTenLoaiSP: TextView = viewGroupCon.findViewById(R.id.txt_tenloaisp)
//            txtTenLoaiSP.setText(listcon.get(groupPosition).mListCon!!.get(childPosition).mTenLoaiSP)
//
//            return viewGroupCon
//        }
//
//        override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
//            return true
//        }
//    }


}