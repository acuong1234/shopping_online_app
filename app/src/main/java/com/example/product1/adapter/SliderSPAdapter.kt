package com.example.product1.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.product1.R
import com.squareup.picasso.Picasso

class SliderSPAdapter(var context:Context,var arrayList: List<String>) : PagerAdapter() {
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return arrayList.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layoutInflater : LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view : View = layoutInflater.inflate(R.layout.image_slider_layout_item,container,false)
        val img : ImageView = view.findViewById(R.id.iv_auto_image_slider)
        val curpage : TextView = view.findViewById(R.id.txt_curpage)
        val numpage : TextView = view.findViewById(R.id.txt_numpage)
        val indicator: LinearLayout = view.findViewById(R.id.ln_indicator)

        Picasso.get().load(arrayList.get(position)).into(img)
//        indicator.visibility = View.VISIBLE
//        curpage.setText((position+1).toString())
//        numpage.setText((arrayList.size+1).toString())

        val vp : androidx.viewpager.widget.ViewPager = container as androidx.viewpager.widget.ViewPager
        vp.addView(view,0)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp : ViewPager = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }
}