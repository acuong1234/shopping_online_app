package com.example.product1.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.product1.R
import com.example.product1.model.ObjectClass.SanPhamGioHang
import com.example.product1.model.ObjectClass.ToVNMoney
import com.squareup.picasso.Picasso

class ListChiTietDHAdapter(var context: Context, var arrayList: ArrayList<SanPhamGioHang>) :
    RecyclerView.Adapter<ListChiTietDHAdapter.ListChiTietDHAdapterVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListChiTietDHAdapterVH {
        return ListChiTietDHAdapterVH(
            LayoutInflater.from(context).inflate(
                R.layout.custom_item_chitiet_donhang,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ListChiTietDHAdapterVH, position: Int) {
        val sanPhamGioHang = arrayList.get(position)
        Picasso.get().load(sanPhamGioHang.mAnhNho).into(holder.img)
        holder.ten.setText(sanPhamGioHang.mTenSP)
        holder.num.setText("x"+sanPhamGioHang.mSoLuongMua)
        holder.price.setText(ToVNMoney().toVNMoneyFunction(sanPhamGioHang.mGia))
    }


    class ListChiTietDHAdapterVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img: ImageView = itemView.findViewById(R.id.img_item_detail)
        val ten: TextView = itemView.findViewById(R.id.txt_name_detail)
        val num: TextView = itemView.findViewById(R.id.txt_num_detail)
        val price: TextView = itemView.findViewById(R.id.txt_price_detail)
    }
}