package com.example.product1.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.product1.R
import com.example.product1.model.ObjectClass.DonHang
import com.example.product1.model.ObjectClass.ToVNMoney
import com.squareup.picasso.Picasso

class ListDonHangApdater(var context : Context, var arrayList: ArrayList<DonHang>, var clickItemList: ClickItemList) : RecyclerView.Adapter<ListDonHangApdater.ListDonHangApdaterVH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListDonHangApdaterVH {
        return ListDonHangApdaterVH(LayoutInflater.from(context).inflate(R.layout.custom_item_list_donhang,parent,false))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ListDonHangApdaterVH, position: Int) {
        val donhang = arrayList.get(position)
        holder.madh.append(donhang.mMaDonHang)
        Picasso.get().load(donhang.mChiTietDonHang.get(0).mAnhNho).into(holder.img)
        holder.ten.setText(donhang.mChiTietDonHang.get(0).mTenSP)
        holder.num.setText("x"+donhang.mChiTietDonHang.get(0).mSoLuongMua)
        holder.price.setText(ToVNMoney().toVNMoneyFunction(donhang.mChiTietDonHang.get(0).mGia))
        holder.total.setText(ToVNMoney().toVNMoneyFunction(donhang.mTongTien))
        when(Integer.parseInt(donhang.mTrangThai)){
            0 -> holder.status.setText("Đã hoàn thành")
            1 -> holder.status.setText("Đang giao hàng")
            2 -> holder.status.setText("Đã xác nhận đơn hàng")
            3 -> holder.status.setText("Đang đợi xác nhận")
        }
        holder.itemView.setOnClickListener {
            clickItemList.onClick(position)
        }
        holder.detail.setOnClickListener {
            clickItemList.onClick(position)
        }
//        if(donhang.mChiTietDonHang.size>0)
//            holder.more.visibility = View.VISIBLE
//        else
//            holder.more.visibility = View.INVISIBLE
    }


    class ListDonHangApdaterVH(itemView: View) : RecyclerView.ViewHolder(itemView){
        val madh : TextView = itemView.findViewById(R.id.txt_ma_don_hang)
        val img : ImageView = itemView.findViewById(R.id.img_first_item)
        val ten : TextView = itemView.findViewById(R.id.txt_name_first)
        val num : TextView = itemView.findViewById(R.id.txt_num_first)
        val price : TextView = itemView.findViewById(R.id.txt_price_first)
//        val more : TextView = itemView.findViewById(R.id.txt_more)
        val total : TextView = itemView.findViewById(R.id.txt_thanh_toan)
        val status : TextView = itemView.findViewById(R.id.txt_status)
        val detail : Button = itemView.findViewById(R.id.btn_detail)
    }
}