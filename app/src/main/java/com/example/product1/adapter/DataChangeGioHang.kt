package com.example.product1.adapter

interface DataChangeGioHang {
    fun dataIncrease(data:Int,pos:Int)
    fun dataDecrease(data:Int,pos:Int)
}