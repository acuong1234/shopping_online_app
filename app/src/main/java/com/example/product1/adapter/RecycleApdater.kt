package com.example.product1.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.product1.R
import com.example.product1.model.ObjectClass.SanPham
import com.example.product1.model.ObjectClass.ToVNMoney
import com.squareup.picasso.Picasso
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


class RecycleApdater(var context: Context?,var arrayList: ArrayList<SanPham>, var clickItemList: ClickItemList) : androidx.recyclerview.widget.RecyclerView.Adapter<RecycleApdater.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_item,p0,false))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        if(p0!=null){
            p0.ten.setText(arrayList.get(p1).mTenSP)
            p0.gia.setText(ToVNMoney().toVNMoneyFunction(arrayList.get(p1).mGia))
            val photo = arrayList.get(p1).mAnhNho
            Picasso.get().load(photo).into(p0.anh)
            p0.itemView.setOnClickListener {
                clickItemList.onClick(p1)
            }
        }
    }
    class ViewHolder(itemView: View?): androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView!!){
        var anh : ImageView = itemView!!.findViewById(R.id.rv_img)
        var ten : TextView = itemView!!.findViewById(R.id.rv_textName)
        var gia : TextView = itemView!!.findViewById(R.id.rv_textGia)
    }
}


