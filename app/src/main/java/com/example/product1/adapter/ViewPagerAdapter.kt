package com.example.product1.adapter

import com.example.product1.view.TrangChu.Fragment.*

class ViewPagerAdapter(fm: androidx.fragment.app.FragmentManager?) : androidx.fragment.app.FragmentStatePagerAdapter(fm) {
    override fun getItem(p0: Int): androidx.fragment.app.Fragment {
        var fragment: androidx.fragment.app.Fragment = androidx.fragment.app.Fragment()
        when(p0){
            0->fragment=SPTheoDanhMuc()
            1->fragment=ChuongTrinhKhuyenMai()
            2->fragment=DienTu()
            3->fragment=NhaCuaDoiSong()
            4->fragment=MeBe()
            5->fragment=LamDep()
            6->fragment=ThoiTrang()
            7->fragment=TheThaoDuLich()
            8->fragment=ThuongHieu()
        }
        return fragment
    }

    override fun getCount(): Int {
        return 9
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title:String=""
        when(position){
            0->title="Nổi bật"
            1->title="Chương trình khuyến mãi"
            2->title="Điện tử"
            3->title="Nhà cửa & Đời sống"
            4->title="Mẹ & bé"
            5->title="Làm đẹp"
            6->title="Thời trang"
            7->title="Thể thao & Du lịch"
            8->title="Thương hiệu"
        }
        return title
    }
}