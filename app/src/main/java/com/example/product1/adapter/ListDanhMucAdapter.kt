package com.example.product1.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.product1.R
import com.example.product1.model.ObjectClass.DanhMuc
import kotlinx.android.synthetic.main.custom_item_list_danhmuc.view.*

class ListDanhMucAdapter(var context:Context,var arrayList: ArrayList<DanhMuc>, var clickItemList: ClickItemList) : androidx.recyclerview.widget.RecyclerView.Adapter<ListDanhMucAdapter.ListDanhMucAdapterVH>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ListDanhMucAdapterVH {
        return ListDanhMucAdapterVH(LayoutInflater.from(context).inflate(R.layout.custom_item_list_danhmuc,p0,false))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(p0: ListDanhMucAdapterVH, p1: Int) {
        p0.name.setText(arrayList.get(p1).mName)
        p0.pic.setImageResource(arrayList.get(p1).mAnh)
        p0.itemView.setOnClickListener {
            clickItemList.onClick(p1)
        }

    }

    class ListDanhMucAdapterVH(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView){
        val name : TextView = itemView.findViewById(R.id.txt_item_danhmuc)
        val pic : ImageView = itemView.findViewById(R.id.img_item_danhmuc)
    }
}